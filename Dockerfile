FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

LABEL maintainer="Evan Wies <evan@neomantra.net>"

ENV RESTY_IMAGE_BASE="centos"
ARG RESTY_LUAROCKS_VERSION="2.4.4"
ARG RESTY_RPM_FLAVOR=""
ARG RESTY_RPM_VERSION="1.13.6.2-1.el7.centos"
ARG RESTY_RPM_ARCH="x86_64"

LABEL resty_luarocks_version="${RESTY_LUAROCKS_VERSION}"
LABEL resty_rpm_flavor="${RESTY_RPM_FLAVOR}"
LABEL resty_rpm_version="${RESTY_RPM_VERSION}"
LABEL resty_rpm_arch="${RESTY_RPM_ARCH}"

RUN yum-config-manager --add-repo https://openresty.org/package/${RESTY_IMAGE_BASE}/openresty.repo \
    && yum install -y \
        gettext \
        make \
        openresty${RESTY_RPM_FLAVOR}-${RESTY_RPM_VERSION}.${RESTY_RPM_ARCH} \
        openresty-opm-${RESTY_RPM_VERSION} \
        openresty-resty-${RESTY_RPM_VERSION} \
        unzip \
        git \
        scl-utils \
    && cd /tmp \
    && curl -fSL https://github.com/luarocks/luarocks/archive/${RESTY_LUAROCKS_VERSION}.tar.gz -o luarocks-${RESTY_LUAROCKS_VERSION}.tar.gz \
    && tar xzf luarocks-${RESTY_LUAROCKS_VERSION}.tar.gz \
    && cd luarocks-${RESTY_LUAROCKS_VERSION} \
    && ./configure \
        --prefix=/usr/local/openresty/luajit \
        --with-lua=/usr/local/openresty/luajit \
        --lua-suffix=jit-2.1.0-beta3 \
        --with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1 \
    && make build \
    && make install \
    && cd /tmp \
    && rm -rf luarocks-${RESTY_LUAROCKS_VERSION} luarocks-${RESTY_LUAROCKS_VERSION}.tar.gz \
    && yum remove -y make \
    && yum clean all \
    && ln -sf /dev/stdout /usr/local/openresty/nginx/logs/access.log \
    && ln -sf /dev/stderr /usr/local/openresty/nginx/logs/error.log

# Unused, present for parity with other Dockerfiles
# This makes some tooling/testing easier, as specifying a build-arg
# and not consuming it fails the build.
ARG RESTY_J="1"

# Add additional binaries into PATH for convenience
ENV PATH=$PATH:/usr/local/openresty/luajit/bin:/usr/local/openresty/nginx/sbin:/usr/local/openresty/bin

# Copy nginx configuration files
COPY nginx.conf /usr/local/openresty/nginx/conf/nginx.conf
COPY nginx.vh.default.conf /etc/nginx/conf.d/default.conf

ENV NAME=nginx \
    NGINX_VERSION=1.12 \
    NGINX_SHORT_VER=112 \
    VERSION=0

ENV SUMMARY="Platform for running nginx $NGINX_VERSION or building nginx-based application" \
    DESCRIPTION="Nginx is a web server and a reverse proxy server for HTTP, SMTP, POP3 and IMAP \
protocols, with a strong focus on high concurrency, performance and low memory usage. The container \
image provides a containerized packaging of the nginx $NGINX_VERSION daemon. The image can be used \
as a base image for other applications based on nginx $NGINX_VERSION web server. \
Nginx server image can be extended using source-to-image tool."

LABEL summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.description="${DESCRIPTION}" \
      io.k8s.display-name="CERN OAuth Reverse Proxy" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,nginx,openresty,proxy" \
      name="cern-oauth-proxy" \
      version="${NGINX_VERSION}" \
      maintainer="gitlab.cern.ch/hse-see-co-docker/cern-oauth-proxy" \
      help="For more information visit gitlab.cern.ch/hse-see-co-docker/cern-oauth-proxy"


ENV APP_ROOT=/opt/app-root \
    NGINX_ROOT=/usr/local/openresty \
    NGINX_CONFIGURATION_PATH=/usr/local/openresty/nginx/conf \
    NGINX_CONF_PATH=/usr/local/openresty/nginx/conf/nginx.conf \
    NGINX_DEFAULT_CONF_PATH=/etc/nginx/conf.d \
    NGINX_CONTAINER_SCRIPTS_PATH=/usr/share/container-scripts/nginx \
    HOME=/opt/app-root

# When bash is started non-interactively, to run a shell script, for example it
# looks for this variable and source the content of this file. This will enable
# the SCL for all scripts without need to do 'scl enable'.
ENV BASH_ENV=${APP_ROOT}/etc/scl_enable \
    ENV=${APP_ROOT}/etc/scl_enable \
    PROMPT_COMMAND=". ${APP_ROOT}/etc/scl_enable"

# Copy extra files to the image.
COPY ./root/ /

# Directory with the sources is set as the working directory so all STI scripts
# can execute relative to this path.
WORKDIR ${HOME}

############################# END First Dockerfile ######################

# Reset permissions of modified directories and add default user
RUN chmod uaog+x /usr/bin/container-entrypoint && \
  chmod +x /usr/bin/fix-permissions && \
  chmod +x /usr/bin/rpm-file-permissions && \
  rpm-file-permissions && \
  useradd -u 1001 -r -g 0 -d ${HOME} -s /sbin/nologin \
      -c "Default Application User" default && \
  chown -R 1001:0 ${NGINX_ROOT}

ENV PORT=8080
#ENV SSL_PORT=8443

EXPOSE ${PORT}
#EXPOSE ${SSL_PORT}

# In order to drop the root user, we have to make some directories world
# writeable as OpenShift default security model is to run the container under
# random UID.
RUN sed -i -f ${APP_ROOT}/nginxconf.sed ${NGINX_CONF_PATH} && \
    chmod a+rwx ${NGINX_CONF_PATH} && \
#    mkdir -p ${NGINX_APP_ROOT}/etc/nginx.d/ && \
#    mkdir -p ${NGINX_APP_ROOT}/etc/nginx.default.d/ && \
#    mkdir -p /var/opt/rh/rh-nginx${NGINX_SHORT_VER}/log/nginx && \
    ln -sf /dev/stdout ${NGINX_ROOT}/nginx/logs/access.log && \
    ln -sf /dev/stderr ${NGINX_ROOT}/nginx/logs/error.log && \
    chmod -R a+rwx ${APP_ROOT}/etc && \
    chown -R 1001:0 ${APP_ROOT} && \
    chown -R 1001:0 ${NGINX_DEFAULT_CONF_PATH} && \
    chown -R 1001:0 ${NGINX_ROOT}

RUN rpm-file-permissions && fix-permissions /etc/nginx && fix-permissions /usr/local/openresty

# Not using VOLUME statement since it's not working in OpenShift Online:
# https://github.com/sclorg/httpd-container/issues/30
# VOLUME ["/opt/rh/rh-nginx112/root/usr/share/nginx/html"]
# VOLUME ["/var/opt/rh/rh-nginx112/log/nginx/"]

##ENV NGO_CALLBACK_HOST
# is the value of $ngo_callback_host.
##ENV NGO_CALLBACK_SCHEME
# is the value of $ngo_callback_scheme.
##ENV NGO_CALLBACK_URI
# is the value of $ngo_callback_uri.
##ENV NGO_SIGNOUT_URI
# is the value of $ngo_signout_uri.
##ENV NGO_CLIENT_ID
# is the value of $ngo_client_id, required.
##ENV NGO_CLIENT_SECRET
# is the value of $ngo_client_secret, required.
##ENV NGO_TOKEN_SECRET
# is the value of $ngo_token_secret, required.
##ENV NGO_SECURE_COOKIES
# is the value of $ngo_secure_cookies.
##ENV NGO_HTTP_ONLY_COOKIES
# is the value of $ngo_http_only_cookies.
##ENV NGO_EXTRA_VALIDITY
# is the value of $ngo_extra_validity.
##ENV NGO_DOMAIN
# is the value of $ngo_domain.
##ENV NGO_WHITELIST
# is the value of $ngo_whitelist.
##ENV NGO_BLACKLIST
# is the value of $ngo_blacklist.
##ENV NGO_USER
# is the value of $ngo_user.
##ENV NGO_EMAIL_AS_USER
# is the value of $ngo_email_as_user.
##ENV PORT
# is the port for nginx to listen on, defaults to 80.
##ENV ENABLE_SSL
# if set, enables 80 redirect + 443 ssl listen. expect PEM certificate for host + key at /etc/nginx/ssl/host.pem and /etc/nginx/ssl/host.key
##ENV DEBUG
# if set, prints the generated nginx configuation on container start
##ENV LOCATIONS

RUN git clone -c transfer.fsckobjects=true https://github.com/pintsized/lua-resty-http.git /tmp/lua-resty-http && \
    cd /tmp/lua-resty-http && \
    # https://github.com/pintsized/lua-resty-http/releases/tag/v0.12 v0.12
    git checkout 607ef6b2fbff5112b9acc47d9163635c2fbd8f59 && \
    mkdir -p ${NGINX_ROOT}/lualib && \
    cp -aR /tmp/lua-resty-http/lib/resty/* ${NGINX_ROOT}/lualib/resty && \
    rm -rf /tmp/lua-resty-http
#
COPY ./access.lua ${NGINX_ROOT}/lualib/nginx-oauth/access.lua
##COPY ./nginx-cfg/* ${NGINX_CONFIGURATION_PATH}
##COPY ./nginx-default-cfg/* ${NGINX_DEFAULT_CONF_PATH}
##COPY ./nginx-start ${NGINX_CONTAINER_SCRIPTS_PATH}
##ADD ./CERN-bundle.pem /etc/ssl/certs/
#RUN chmod ugoa+r /etc/ssl/certs/CERN-bundle.pem
#RUN chmod ugoa+r -R ${NGINX_APP_ROOT}/lua
# for SSL proxy only. can be removed if not needed
USER 1001
WORKDIR ${HOME}
ENTRYPOINT ["container-entrypoint"]
